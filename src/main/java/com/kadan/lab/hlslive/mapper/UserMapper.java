package com.kadan.lab.hlslive.mapper;

import com.kadan.lab.hlslive.Entity.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface UserMapper {

    @Insert("INSERT INTO t_users(id,username,password,role,reserve1,reserve2) VALUES(#{id},#{username},#{password},#{role},#{reserve1},#{reserve2})")
    int insert(User model);

    //get all users
    @Select("SELECT * FROM t_users")
    List<User> selectAll();

    //get user by name
    @Select("SELECT * FROM t_users WHERE username=#{username}")
    User select(String username);


    @Delete("DELETE FROM t_users WHERE username=#{username}")
    int deleteByName(String username);

    @Delete("DELETE FROM t_users WHERE id=#{id}")
    int deleteByID(String id);

    @Update("UPDATE t_users SET password=#{password} WHERE username=#{name}")
    int updatePassword(String password, String name);
}
