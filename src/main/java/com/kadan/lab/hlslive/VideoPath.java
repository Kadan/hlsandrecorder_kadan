package com.kadan.lab.hlslive;

import org.springframework.beans.factory.annotation.Value;

public enum  VideoPath {


    //存储地址以及代号
    ONE(1,"/Users/lijunjie/Downloads/");
    private int num;
    private String name;
    private VideoPath(int num, String name){
        this.num = num;
        this.name = name;
    }



    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}