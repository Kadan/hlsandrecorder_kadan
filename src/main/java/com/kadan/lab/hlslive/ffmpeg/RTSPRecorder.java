package com.kadan.lab.hlslive.ffmpeg;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class RTSPRecorder {


    @Autowired
    FFmpeg ffmpeg;

    @Autowired
    List<String> pusherList;

    @Autowired
    FFprobe ffprobe;

    @Value("${videopath}")
    private String videopath;

    @Value("${recordLength}")
    private int recordLength;


//    @Value("${darwinUserPwd}")
//    private String darwinUserPwd;

    private static ThreadFactory pushRTMPFactory = new ThreadFactory() {
        private final AtomicInteger counter = new AtomicInteger();
        @Override
        public Thread newThread(Runnable runable) {
            return new Thread(runable, "### thread ### " + counter.getAndIncrement());
        }
    };

    /**prepare the threadpool*/
    private static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(300, 2000, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100),pushRTMPFactory);



    public void record_perDay(String inputRTSP, String outputFile){

        System.out.println("recording " +  inputRTSP + "  to " + outputFile);
        threadPoolExecutor.submit(
                new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        //Thread.sleep(200);
                        record(inputRTSP,outputFile);
                        return 0;
                        //return sendSMSMsg(s)==true?0:1;
                    }
                });
    }


    public void record_perHour(String inputRTSP, String outputFile){

        System.out.println("recording " +  inputRTSP + "  to " + outputFile);
        threadPoolExecutor.submit(
                new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        //Thread.sleep(200);
                        recordperhour(inputRTSP,outputFile);
                        return 0;
                        //return sendSMSMsg(s)==true?0:1;
                    }
                });
    }

    public void record_perMin(String inputRTSP, String outputFile){
        System.out.println("recording " +  inputRTSP + "  to " + outputFile);
        threadPoolExecutor.submit(
                new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        //Thread.sleep(200);
                        recordperMin(inputRTSP,outputFile);
                        return 0;
                        //return sendSMSMsg(s)==true?0:1;
                    }
                });
    }


    public void recordperhour(String inputRTSP, String outputFile) {
        try{
            StringBuilder sb = new StringBuilder(inputRTSP);
            sb.insert(7, "");

            FFmpegBuilder builder1 =
                    new FFmpegBuilder()
                            .setInput(sb.toString())
                            .overrideOutputFiles(false)
                            .addOutput(outputFile)
                            .setDuration(58, TimeUnit.MINUTES)
                            .setVideoCodec("copy")
                            .setAudioCodec("copy")
                            .setAudioChannels(1)
                            .setAudioBitStreamFilter("aac_adtstoasc")
                            .setFormat("mp4")
                            .done();
            new FFmpegExecutor(ffmpeg,ffprobe).createJob(builder1).run();
        }catch(Exception e){
            e.printStackTrace();
        }

    }


    //per day
    // @Scheduled(cron = "0 0 0 * * ?")
    public void record(String inputRTSP, String outputFile) {
        try{
            FFmpegBuilder builder1 =
                    new FFmpegBuilder()
                            .setInput(inputRTSP)
                            .overrideOutputFiles(false)
                            .addOutput(outputFile)
                            .setDuration(1438, TimeUnit.MINUTES)
                            .setVideoCodec("copy")
                            .setAudioCodec("copy")
                            .setAudioChannels(1)
                            .setAudioBitStreamFilter("aac_adtstoasc")
                            .setFormat("mp4")
                            .done();
            new FFmpegExecutor(ffmpeg,ffprobe).createJob(builder1).run();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    //per min
    public void recordperMin(String inputRTSP, String outputFile) {
        try{
            FFmpegBuilder builder1 =
                    new FFmpegBuilder()
                            .setInput(inputRTSP)
                            .overrideOutputFiles(false)
                            .addOutput(outputFile)
                            .setDuration(58, TimeUnit.SECONDS)
                            .setVideoCodec("copy")
                            .setAudioCodec("copy")
                            .setAudioChannels(1)
                            .setAudioBitStreamFilter("aac_adtstoasc")
                            .setFormat("mp4")
                            .setAudioChannels(1)
                            .setAudioBitRate(32768)
                            .setAudioSampleRate(48_000)
                            .done();
            new FFmpegExecutor(ffmpeg,ffprobe).createJob(builder1).run();
        }catch(Exception e){
            e.printStackTrace();
        }

    }


    public void recording(String inputRTSP,String darwinUserPwd){

        System.out.println("recording " +  inputRTSP );
        threadPoolExecutor.submit(
                new Callable<Integer>() {
                    @Override
                    public Integer call(){

                        try{
                        //Thread.sleep(200);
                        recordS(inputRTSP,darwinUserPwd);
                        }catch(Exception e){
                            pusherList.remove(inputRTSP);
                        }
                        return 0;
                    }
                });
    }


    public void recordS(String inputRTSP,String darwinUserPwd) {
        System.out.println("recording for "+ inputRTSP);
        String recordDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyy-MM-dd-HH-mm-ss"));
        String path = StringUtils.substringAfterLast(inputRTSP,"/");
        String id = StringUtils.substringBefore(path,".sdp");
        String parentFolderStr = videopath + id;
        File parentFolder = new File(parentFolderStr);
        if(!parentFolder.exists()){parentFolder.mkdir();parentFolder.setWritable(true);}
        String day = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyy-MM-dd"));
        String dayFolderStr = parentFolderStr + "/" + day;
        File dayFolder = new File(dayFolderStr);
        if(!dayFolder.exists()){dayFolder.mkdir();dayFolder.setWritable(true);}
        String outputFile = dayFolderStr + "/"+recordDateTime + ".mp4";
        try{
            StringBuilder sb = new StringBuilder(inputRTSP);
            sb.insert(7, darwinUserPwd);
            FFmpegBuilder builder1 =
                    new FFmpegBuilder()
                            .setInput(sb.toString())
                            .overrideOutputFiles(false)
                            .addOutput(outputFile)
                            .setDuration(recordLength, TimeUnit.MINUTES)
                            .setVideoCodec("copy")
                            .setAudioCodec("copy")
                            .setAudioChannels(1)
                            .setAudioBitStreamFilter("aac_adtstoasc")
                            .setFormat("mp4")
                            .setAudioChannels(1)
                            .setAudioBitRate(32768)
                            .setAudioSampleRate(48_000)
                            .done();
            new FFmpegExecutor(ffmpeg,ffprobe).createJob(builder1).run();
            pusherList.remove(inputRTSP);
        }catch(Exception e){
            e.printStackTrace();
            pusherList.remove(inputRTSP);
        }finally {
            pusherList.remove(inputRTSP);
        }
    }
}
