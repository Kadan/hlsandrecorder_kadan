package com.kadan.lab.hlslive.ffmpeg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class DarwinListener implements ApplicationListener<DarwinChangeEvent> {

    @Autowired
    private RTSPRecorder restRecorder;

    @Override
    public void onApplicationEvent(DarwinChangeEvent darwinChangeEvent) {
        System.out.println("### darwin listener will be executing the recording ### ");
        darwinChangeEvent.getList().stream().forEach(s->restRecorder.record(s,""));
    }
}
