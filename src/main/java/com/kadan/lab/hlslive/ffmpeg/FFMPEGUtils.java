package com.kadan.lab.hlslive.ffmpeg;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class FFMPEGUtils {


    @Autowired
    FFmpeg ffmpeg;

    @Autowired
    FFprobe ffprobe;

//    @Autowired
//    Map<String,String> pusherMap;



    @Autowired
    List<String> liveList;

    private static ThreadFactory pushRTMPFactory = new ThreadFactory() {
        private final AtomicInteger counter = new AtomicInteger();

        @Override
        public Thread newThread(Runnable runable) {
            return new Thread(runable, "### thread ### " + counter.getAndIncrement());
        }
    };

    /**
     * prepare the threadpool
     */
    private static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(300, 2000, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100), pushRTMPFactory);


    public void push(String inputRTSP,String username, String outputRTMP,String darwinUserPwd) {

        System.out.println("pushing " + inputRTSP + "  to " + outputRTMP);
        threadPoolExecutor.submit(
                new Callable<Integer>() {
                    @Override
                    public Integer call() {
                        //Thread.sleep(200);
                        push2RTMPServer(inputRTSP,username, outputRTMP,darwinUserPwd);
                        return 0;
                        //return sendSMSMsg(s)==true?0:1;
                    }
                });
    }


    public void push2RTMPServer(String inputRTSP, String username, String outputRTMP,String darwinUserPwd) {


        try {
            //RunProcessFunction func = new RunProcessFunction();
            //func.setWorkingDirectory("/Users/lijunjie/easyDarwin/ffmpeg/bin");
            //
            //FFmpeg ffmpeg = new FFmpeg(ffmpegpath);
            //FFprobe ffprobe = new FFprobe("/usr/local/bin/ffprobe", func);
            StringBuilder sb = new StringBuilder(inputRTSP);
            sb.insert(7, username+":"+StringUtils.substringAfter(darwinUserPwd,"pass=")+"@");

            FFmpegBuilder builder =
                    new FFmpegBuilder()
//                        .readAtNativeFrameRate()
                            //.setInput("rtsp://139.219.142.18/test0.sdp")
                            .setInput(sb.toString())
                            //.addExtraArgs("-bsf:a aac_adtstoasc")
                            //.addExtraArgs("-bsf:a aac_adtstoasc")
                            //.addExtraArgs("-c:v copy")
                            //.setAudioCodec("libmp3lame")

                            //.addExtraArgs("-bsf:a aac_adtstoasc")
                            //.setVideoCodec("libx264")

                            //.setVideoCodec("libx264")


                            //.setPreset("ultrafast")
                            //.setConstantRateFactor(20)
                            //.setAudioCodec("libmp3")
                            //.setAudioFilter("aac_adtstoasc")

                            //.addOutput("rtmp://120.79.159.245:9090/hls/test")
                            .addOutput(outputRTMP+darwinUserPwd)
//                            .addOutput(outputRTMP)
                            .setVideoCodec("copy")
                            .setAudioCodec("copy")
                            .setAudioChannels(1)
                            //.addExtraArgs("bsf:a aac_adtstoasc")
                            //.setAudioFilter(".addExtraArgs(\"-bsf:a aac_adtstoasc\")")
                            //.setAudioFilter("-bsf:a aac_adtstoasc")
                            //.setFormat("flv")
                            //.setAudioFilter("adtstoasc")
                            .setAudioBitStreamFilter("aac_adtstoasc")
                            .setFormat("flv")
//                        .addExtraArgs("-bsf:a aac_adtstoasc")
                            .done();

            FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);
            executor.createJob(builder).run();
            //executor.wait();

            liveList.remove(inputRTSP);

            // return true;
        } catch (Exception e) {
            e.printStackTrace();
            //if(!StringUtils.containsAny(e.getMessage(),"lready publishing"))
            liveList.remove(inputRTSP);
        }finally{
            liveList.remove(inputRTSP);
        }

    }
}
