package com.kadan.lab.hlslive.ffmpeg;

import org.springframework.context.ApplicationEvent;

import java.util.List;

public class DarwinChangeEvent extends ApplicationEvent {
    private static final Long serialVersionUID = 1l;

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    private List<String> list;


    public DarwinChangeEvent(Object source){
        super(source);
    }


    public DarwinChangeEvent(Object source,List<String> list){
        super(source);
        this.list = list;
    }

}
