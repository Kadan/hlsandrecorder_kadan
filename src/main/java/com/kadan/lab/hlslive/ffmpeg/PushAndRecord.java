//package com.kadan.lab.hlslive.ffmpeg;
//
//import org.bytedeco.javacpp.Loader;
//import org.bytedeco.javacpp.avcodec;
//import org.bytedeco.javacpp.opencv_core;
//import org.bytedeco.javacpp.opencv_objdetect;
//import org.bytedeco.javacv.*;
//
//import javax.swing.*;
//
//public class PushAndRecord {
//    /**
//     *
//     *
//     * @author eguid
//     * @param outputFile -录制的文件路径，也可以是rtsp或者rtmp等流媒体服务器发布地址
//     * @param frameRate - 视频帧率
//     * @throws Exception
//     * @throws InterruptedException
//     * @throws org.bytedeco.javacv.FrameRecorder.Exception
//     */
//    public void recordCamera(String outputFile, double frameRate)
//            throws Exception, InterruptedException, org.bytedeco.javacv.FrameRecorder.Exception {
//        Loader.load(opencv_objdetect.class);
//        FrameGrabber grabber = FrameGrabber.createDefault(0);//本机摄像头默认0，这里使用javacv的抓取器，至于使用的是ffmpeg还是opencv，请自行查看源码
//        grabber.start();//开启抓取器
//
//        OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();//转换器
//        opencv_core.IplImage grabbedImage = converter.convert(grabber.grab());//抓取一帧视频并将其转换为图像，至于用这个图像用来做什么？加水印，人脸识别等等自行添加
//        int width = grabbedImage.width();
//        int height = grabbedImage.height();
//
//        FrameRecorder recorder = FrameRecorder.createDefault(outputFile, width, height);
//        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264); // avcodec.AV_CODEC_ID_H264，编码
//        recorder.setFormat("flv");//封装格式，如果是推送到rtmp就必须是flv封装格式
//        recorder.setFrameRate(frameRate);
//
//        recorder.start();//开启录制器
//        long startTime=0;
//        long videoTS=0;
//        CanvasFrame frame = new CanvasFrame("camera", CanvasFrame.getDefaultGamma() / grabber.getGamma());
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame.setAlwaysOnTop(true);
//        Frame rotatedFrame=converter.convert(grabbedImage);//不知道为什么这里不做转换就不能推到rtmp
//        while (frame.isVisible() && (grabbedImage = converter.convert(grabber.grab())) != null) {
//            rotatedFrame = converter.convert(grabbedImage);
//            frame.showImage(rotatedFrame);
//            if (startTime == 0) {
//                startTime = System.currentTimeMillis();
//            }
//            videoTS = 1000 * (System.currentTimeMillis() - startTime);
//            recorder.setTimestamp(videoTS);
//            recorder.record(rotatedFrame);
//            Thread.sleep(40);
//        }
//        frame.dispose();
//        recorder.stop();
//        recorder.release();
//        grabber.stop();
//
//    }
//
//    /**
//     * 转流器
//     * @param inputFile
//     * @param outputFile
//     * @throws Exception
//     * @throws org.bytedeco.javacv.FrameRecorder.Exception
//     * @throws InterruptedException
//     */
//
//    public void pushAndRecord(String inputFile,String outputFile,int v_rs)throws Exception, org.bytedeco.javacv.FrameRecorder.Exception, InterruptedException{
//        //Loader.load(opencv_objdetect.class);
//        long startTime=0;
//        FrameGrabber grabber =FFmpegFrameGrabber.createDefault(inputFile);
//        grabber.delayedGrab(10);
//
//       grabber.setAudioCodec(avcodec.AV_CODEC_ID_AAC);
//       grabber.setOption("rtsp_transport", "tcp");
//        grabber.setAudioCodec(0);
//      grabber.setAudioOption("crf", "0");
//        grabber.setSampleRate(44100);
//        grabber.setAudioBitrate(192000);
//        grabber.setAudioCodec(avcodec.AV_CODEC_ID_AAC);
//        grabber.setAudioChannels(1);
////        grabber.setAudioBitrate(16);
////        grabber.setSampleRate(40000);
////        //grabber.setVideoCodec(avcodec.AV_CODEC_ID_H264);
//        int width = 640,height = 480;
//        grabber.setImageHeight(height);
//        grabber.setImageWidth(width);
//        try {
//            grabber.start();
//        } catch (Exception e) {
//            e.printStackTrace();
//            try {
//                grabber.restart();
//            } catch (Exception e1) {
//                throw e1;
//                }
//
//        }
//
//        OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();
//        Frame grabframe =grabber.grab();
//        opencv_core.IplImage grabbedImage =null;
//        if(grabframe!=null){
//            System.out.println("Got the head frame");
//            grabbedImage = converter.convert(grabframe);
//        }else{
//            System.out.println("No head frame");
//        }
//        //如果想要保存图片,可以使用 opencv_imgcodecs.cvSaveImage("hello.jpg", grabbedImage);来保存图片
//        FrameRecorder recorder;
//        try {
//            recorder = FrameRecorder.createDefault(outputFile, 640, 480);
//        } catch (org.bytedeco.javacv.FrameRecorder.Exception e) {
//            throw e;
//        }
//
//
//        //recorder.setAudioCodec(avcodec.AV_CODEC_ID_AAC);
//        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264); // avcodec.AV_CODEC_ID_H264
//        //recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264); // avcodec.AV_CODEC_ID_H264
//        //recorder.setAudioCodec(avcodec.AV_CODEC_ID_MP3);
//        System.out.println("-----" + recorder.getAudioCodecName());
//        recorder.setFormat("flv");
//        recorder.setFrameRate(v_rs);
//        recorder.setGopSize(v_rs);
//        System.out.println("ready for pushing...");
//        try {
//            recorder.start();
//        } catch (org.bytedeco.javacv.FrameRecorder.Exception e) {
//            try {
//                System.out.println("restarting the recorder...");
//                if(recorder!=null)
//                {
//                    System.out.println("shutting down the recorder");
//                    recorder.stop();
//                    System.out.println("restarting the recorder");
//                    recorder.start();
//                }
//
//            } catch (org.bytedeco.javacv.FrameRecorder.Exception e1) {
//                throw e;
//            }
//        }
//        System.out.println("pushing start");
//        //CanvasFrame frame = new CanvasFrame("camera", CanvasFrame.getDefaultGamma() / grabber.getGamma());
//        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        //frame.setAlwaysOnTop(true);
//        while ((grabframe=grabber.grab()) != null) {
//            System.out.println("pushing...");
//            //frame.showImage(grabframe);
//            grabbedImage = converter.convert(grabframe);
//            Frame rotatedFrame = converter.convert(grabbedImage);
//
//            if (startTime == 0) {
//                startTime = System.currentTimeMillis();
//            }
//            recorder.setTimestamp(1000 * (System.currentTimeMillis() - startTime));//timestamp
//            if(rotatedFrame!=null){
//                recorder.record(rotatedFrame);
//            }
//
//            Thread.sleep(40);
//        }
//        //frame.dispose();
//        recorder.stop();
//        recorder.release();
//        grabber.stop();
//        //System.exit(2);
//    }
//
//}
