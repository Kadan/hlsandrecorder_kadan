package com.kadan.lab.hlslive;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.File;

@RestController
public class FetchVideoPath {

    @Value("${videopath}")
    private String videopath;

    /**
     * 获取文件夹下所有文件
     * @return
     */
    @GetMapping("/VideoPath")
    public String requireVideoPath(){
        String path = null;
        String[] paths = null;
        //使用枚举代替常量
        //File rootFile = new File(VideoPath.ONE.getName());
        File rootFile = new File(videopath);
        File[] files = rootFile.listFiles();
        paths = new String[files.length];
        for(int i =0;i<files.length;i++){
            File childrenFile = files[i];
            if(childrenFile.length()>0){
            //System.out.println(childrenFile.getPath());
            //if(childrenFile.getName() != null && childrenFile.getName().contains(".flv"))
            paths[i] = childrenFile.getName();}
        }
        //System.out.println(JSONObject.toJSONString(paths));
        path = JSONObject.toJSONString(paths);
        return path;
    }
//    public static void main(String[] args){
//        requireVideoPath();
//    }
}
