package com.kadan.lab.hlslive.Service;

import com.kadan.lab.hlslive.Entity.User;
import com.kadan.lab.hlslive.mapper.UserMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class UserService {
    private final UserMapper dao;

    @Autowired
    public UserService(UserMapper dao) {
        this.dao = dao;
    }

    public boolean insert(User model) {
        return dao.insert(model)>0;
    }

    public User select(String id) {
        return dao.select(id);
    }

    public List<User> selectAll() {
        return dao.selectAll();
    }

    public boolean updatePassword(String password,String name) {
        return dao.updatePassword(password,name)>0;
    }

    public boolean delete(String id) {
        return dao.deleteByID(id)>0;
    }

    public boolean deleteByName(String username) {
        return dao.deleteByName(username)>0;
    }

}
