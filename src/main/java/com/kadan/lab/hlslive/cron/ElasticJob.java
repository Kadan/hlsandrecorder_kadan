package com.kadan.lab.hlslive.cron;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.dangdang.elasticjob.lite.annotation.ElasticSimpleJob;
import com.kadan.lab.hlslive.Entity.User;
import com.kadan.lab.hlslive.PushController;
import com.kadan.lab.hlslive.util.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@ElasticSimpleJob(cron = "0/3 * * * * ?", jobName = "darwinjob", shardingTotalCount = 16, jobParameter = "darwinparameter", shardingItemParameters = "0=A,1=B,2=C")
@Component
public class ElasticJob implements SimpleJob {

    @Autowired
    List<String> darwinList;
    @Autowired
    PushController pushController;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    User user;


    @Override
    public void execute(ShardingContext shardingContext) {
        int shards = shardingContext.getShardingTotalCount();
        //List<List<String>> lists = new ListUtils().averageAssign(darwinList,shards);
        List<String> allStream = new ArrayList<String>();
        darwinList.stream().forEach(s -> {
            String json = restTemplate.getForObject(s+"?pass="+user.getPassword(), String.class);
            JSONObject jsonObject = JSONObject.parseObject(json);
            int total = jsonObject.getIntValue("total");
            if (total > 0) {
                JSONArray jSONArray = jsonObject.getJSONArray("rows");
                jSONArray.stream().map(jo -> {
                    JSONObject jo1 = (JSONObject) jo;
                    return jo1;
                }).forEach(s1 -> allStream.add(s1.getString("source")));
            }
        });
        Collections.sort(allStream);
        List<List<String>> lists = new ListUtils().averageAssign(allStream,shards);
        int current = shardingContext.getShardingItem();
        List<String> currentList = lists.get(current);
        String darwinUserPwd = user.getUsername() + ":" + user.getPassword() + "@";
        //currentList.stream().forEach(s->System.out.println(Thread.currentThread().getId() + " >>> "  +  current  + "current handling --> " + s));

        currentList.stream().forEach(s->{
            pushController.push(s,user.getUsername(),"?pass="+user.getPassword());
            pushController.recordStream(s,darwinUserPwd);
        });

    }
}
