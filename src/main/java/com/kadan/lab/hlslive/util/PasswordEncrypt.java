package com.kadan.lab.hlslive.util;


import java.security.MessageDigest;
import java.util.Base64;


public class PasswordEncrypt {

    public static String encodeByMd5(String text) throws Exception {
        //加密后的字符串
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        Base64.Encoder base64Encoder = Base64.getEncoder();
        return base64Encoder.encodeToString(md5.digest(text.getBytes("utf-8")));
    }
}
