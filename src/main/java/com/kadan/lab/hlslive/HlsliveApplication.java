package com.kadan.lab.hlslive;

import com.kadan.lab.hlslive.Entity.User;
import com.kadan.lab.hlslive.Service.UserService;
import com.kadan.lab.hlslive.controller.UserController;
import com.kadan.lab.hlslive.ffmpeg.FFMPEGUtils;
import com.kadan.lab.hlslive.ffmpeg.RTSPRecorder;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFprobe;
import net.unicon.cas.client.configuration.EnableCasClient;
import org.jasig.cas.client.authentication.AuthenticationFilter;
import org.jasig.cas.client.session.SingleSignOutFilter;
import org.jasig.cas.client.session.SingleSignOutHttpSessionListener;
import org.jasig.cas.client.util.HttpServletRequestWrapperFilter;
import org.jasig.cas.client.validation.Cas30ProxyReceivingTicketValidationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@SpringBootApplication
@Configuration
@ComponentScan
@EnableScheduling
@EnableAsync
@EnableCasClient
public class HlsliveApplication {


	@Value("${ffmpegpath}")
	private String ffmpegpath;

	@Value("${ffprobepath}")
	private String ffprobepath;

	@Value("${darwinhost}")
	private String darwinHost;
	@Value("${rtmpstatservers}")
	private String liveStreamList;

	@Bean
	public List<String> darwinList(){
		String[] easyDarwinHosts = darwinHost.split(",");
		return Arrays.stream(easyDarwinHosts).distinct().collect(Collectors.toList());
	}

	@Bean
	public List<String> liveStreamList(){
		String[] liveStreams = liveStreamList.split(",");
		return Arrays.stream(liveStreams).distinct().collect(Collectors.toList());
	}


//	@Bean
//	public PushController pushController(){ return  new PushController();}

	@Bean
	public FFmpeg ffmpeg(){

		FFmpeg ffmpeg  = null;
		try {
			ffmpeg = new FFmpeg(ffmpegpath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ffmpeg;
	}

	@Bean
	FFprobe ffprobe() {
		FFprobe ffprobe = null;

		try {
			ffprobe = new FFprobe(ffprobepath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ffprobe;
	}


	@Bean
	public RestTemplate restTemplate(){ return new RestTemplate();}

	@Bean
	public List<String> pusherList(){return new ArrayList<String>();
	}

	@Bean
	public List<String> liveList(){return new ArrayList<String>();
	}

	@Bean
	public FFMPEGUtils ffMPEGUtils(){return new FFMPEGUtils();}

	@Bean
	public RTSPRecorder rtspRecorder(){return new RTSPRecorder();}

	public static void main(String[] args) {
		SpringApplication.run(HlsliveApplication.class, args);

	}

	@Autowired
	UserService userService;
	@Bean
	public User user(){
		User user =  new UserController(userService).theFirst();
		return user;
	}


//	@Value("${elaticjob.zookeeper.server-lists}")
//	private String zookeeperURL;
//
//	private static final int timeout = 3000;


//	@Value("${rtmpCredentials: }")
//	private String rtmpCredentials;
//	@Value("${liveCredentials: }")
//	private String liveCredentials;
//
//	@Bean
//	ZooKeeper zooKeeper(){
//		CountDownLatch cdl = new CountDownLatch(1);
//		ZooKeeper zooKeeper = null;
//		try {
//			zooKeeper = new ZooKeeper(zookeeperURL, timeout, new MyZkWatcher(cdl,"ilasvcZKConn"));
//			//Prepare the passwords for pushing and living
//			List<ACL> acl = ZooDefs.Ids.OPEN_ACL_UNSAFE;
//			CreateMode createMode = CreateMode.EPHEMERAL;
//			zooKeeper.create("/rtmp", rtmpCredentials.getBytes(), acl, createMode);
//			zooKeeper.create("/live", liveCredentials.getBytes(), acl, createMode);
//		} catch (IOException | KeeperException | InterruptedException e) {
//			e.printStackTrace();
//		}
//		return zooKeeper;
//	}


//	private class MyZkWatcher implements Watcher {
//
//		private CountDownLatch cdl;
//		private String mark;
//		public MyZkWatcher(CountDownLatch cdl,String mark) {
//			this.cdl = cdl;
//			this.mark = mark;
//		}
//
//		@Override
//		public void process(WatchedEvent watchedEvent) {
//			cdl.countDown();
//		}
//	}


}
