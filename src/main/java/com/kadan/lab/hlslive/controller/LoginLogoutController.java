package com.kadan.lab.hlslive.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RestController
public class LoginLogoutController {

    @Value("${cas-logout-url}")
    private String casLogoutUrl;

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpSession httpSession, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, ModelMap modelMap) throws IOException {

        httpSession.removeAttribute("musersession");
        httpSession.removeAttribute("_const_cas_assertion_");
        httpSession.invalidate();
        httpServletResponse.sendRedirect(casLogoutUrl);
    }

    @RequestMapping(value="/login")
    public String requestMethodName() {
        return "/index";
    }

//    @RequestMapping("/logoutCustom")
//    public String logout2(HttpSession session) {
//        session.invalidate();
//        // 退出登录后，跳转到退出成功的页面，不走默认页面
//        return "redirect:https://casserver.com:8443/logout?service=http://springbootcasclient.com:8001/logout/success";
//    }
//
//    @RequestMapping("/logout/success")
//    @ResponseBody
//    public String logout2() {
//        return "logout successfully";
//    }


}
