package com.kadan.lab.hlslive.controller;

import com.kadan.lab.hlslive.Entity.User;
import com.kadan.lab.hlslive.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

@RestController
public class UserController {

    @Resource(name = "userService")
    private final UserService service;


    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @RequestMapping(value = "/getAllUsers", method = RequestMethod.GET)
    public List<User> allUsers() {
        return service.selectAll();
    }

    public User theFirst() {
        return service.selectAll().get(0);
    }

    @RequestMapping(value = "/createUser", method = RequestMethod.POST)
    @Transactional
    public String createUser(@RequestParam("name")String name,@RequestParam("password")String password) throws Exception {

        User user = new User();
        //String password = "ilabservice123456";
        user.setId(UUID.randomUUID().toString());
        user.setUsername(name);
        user.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));
        if(service.insert(user)){
            return "user " + user +  "created";
        }else
            return "Could not create the user";

    }

    @RequestMapping(value = "/deleteuser", method = RequestMethod.POST)
    @Transactional
    public String deleteUser() throws Exception {
        if(service.deleteByName("ilabservice")){ return "successfully delete user ";}
                else return "fail to delete user" ;
    }

    @RequestMapping(value = "user/{name}",method = RequestMethod.GET)
    public User getUserByName(@PathVariable("name") String name){
        return service.select(name);
    }

    @RequestMapping(value = "/updateUserPassword", method = RequestMethod.POST)
    @Transactional
    public String updateUserPassword(@RequestParam("name")String name,@RequestParam("password")String password){

            if(StringUtils.isEmpty(name)|| StringUtils.isEmpty(password)){
                return "user name / password can not be empty ...";

            }

            if(service.updatePassword(DigestUtils.md5DigestAsHex(password.getBytes()),name))
                return " User password updated " + name;
            else
                return "fail to update the user passwor " + name;
    }

}
