package com.kadan.lab.hlslive.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
public class LiveStreamsController {

    @Autowired
    RestTemplate restTemplate;


    @Autowired
    List<String> liveStreamList;

        @RequestMapping(value="/getLiveStreams",method = RequestMethod.GET)
        public List<String> getActiveStream() {


        List<String> liveList = new ArrayList<String>();


//        List<String> liveStreamList = liveStreamList

            liveStreamList.stream().distinct().forEach(s->{
        String xml = new RestTemplate().getForObject(s, String.class);
        org.json.JSONArray jsonArray = org.json.XML.toJSONObject(xml).getJSONObject("rtmp").getJSONObject("server")
                .getJSONArray("application");
        for (int i = 0; i < jsonArray.length(); i++) {

            int clients = jsonArray.getJSONObject(i).getJSONObject("live").getInt("nclients");
            if (clients <= 0)
                continue;
            org.json.JSONObject liveObject = jsonArray.getJSONObject(i).getJSONObject("live");
            String hlsname = jsonArray.getJSONObject(i).getString("name");
//            System.out.println("name " + hlsname);
            try {

                if (clients == 1) {
                    org.json.JSONObject jsonObject = liveObject.getJSONObject("stream");

                    String output = jsonObject.getString("name");
                    System.out.println("output" + output);
//                    liveMap.put(hlsname, output);
                    liveList.add(StringUtils.substringBefore(s,"/stat")+"/hls/"+output+"/index.m3u8");
                } else {

                    org.json.JSONArray jsonArray1 = liveObject.getJSONArray("stream");
                    for (int m = 0; m < jsonArray1.length(); m++) {
                        String output = jsonArray1.getJSONObject(m).getString("name");
                        liveList.add(StringUtils.substringBefore(s,"/stat")+"/hls/"+output+"/index.m3u8");
//                    System.out.println("output >>> " + output);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }});
        return liveList;
    }
}
