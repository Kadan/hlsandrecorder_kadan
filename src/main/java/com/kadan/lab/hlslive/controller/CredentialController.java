package com.kadan.lab.hlslive.controller;


import com.kadan.lab.hlslive.Entity.User;
import com.kadan.lab.hlslive.Service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class CredentialController {


    private final UserService service;
    @Autowired
    public CredentialController(UserService service) {
        this.service = service;
    }

//    @Value("${rmtppusher}")
//    private String rmtppusher;

    @ResponseBody
    @RequestMapping(path = "/verifyPlainPwd",method = RequestMethod.POST)
    public ResponseEntity verifyPlainPwd(@RequestParam("name")String name,@RequestParam("password")String password){
            User user = service.select(name);
            if(user == null || StringUtils.isBlank(name) || StringUtils.isBlank(password))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(" user name can not be empty, or user does not exist");
            else {
                if (StringUtils.equalsAnyIgnoreCase(user.getPassword(), DigestUtils.md5DigestAsHex(password.getBytes())))
                    return ResponseEntity.status(HttpStatus.OK).body("ok");
                else
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("verify failed");
            }
    }


    @ResponseBody
    @RequestMapping(path = "/verifyAccess",method = RequestMethod.GET)
    public void verifyAccess(HttpServletResponse response){
        try {
            response.sendRedirect("www.baidu.com");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @ResponseBody
    @RequestMapping(path = "/verify",method = RequestMethod.POST)
    public ResponseEntity verifyPlainPwd(@RequestParam("pass")String password){
        List<User> user = service.selectAll();
        if(user ==null || StringUtils.isBlank(password)) ResponseEntity.status(HttpStatus.BAD_REQUEST).body("verify failed");
        if(user.stream().anyMatch(u->u.getPassword().equalsIgnoreCase(password)))
            return ResponseEntity.status(HttpStatus.OK).body("ok");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("verify failed");
    }


    @ResponseBody
    @RequestMapping(path = "/verifyPwd",method = RequestMethod.POST)
    public ResponseEntity verify(@RequestParam("name")String name,@RequestParam("password")String password){
        User user = service.select(name);
        if(user == null || StringUtils.isBlank(name) || StringUtils.isBlank(password))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(" user name can not be empty, or user does not exist");
        else
            if(StringUtils.equalsAnyIgnoreCase(user.getPassword(), password))
                return ResponseEntity.status(HttpStatus.OK).body("ok");
        else
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("verify failed");

    }


}
