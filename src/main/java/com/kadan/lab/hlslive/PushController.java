package com.kadan.lab.hlslive;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.kadan.lab.hlslive.ffmpeg.FFMPEGUtils;
import com.kadan.lab.hlslive.ffmpeg.RTSPRecorder;
import org.springframework.context.ApplicationContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import java.util.*;

@RestController
@Component
public class PushController {

    @Value("${videopath}")
    private String videopath;

    @Value("${darwinhost}")
    private String darwinHost;

    @Value("${rtmpserver}")
    private String rtmpserver;

    @Autowired
    FFMPEGUtils ffMPEGUtils;

    @Autowired
    RTSPRecorder respRecorder;

    @Autowired
    RestTemplate restTemplate;

//    @Value("${rtmpstat}")
//    private String rmtpstat;
    //rtmp://139.219.142.18/stat

//    @Autowired
//    Map<String,String> pusherMap;


    public List<String> tobeRecord = new ArrayList<String>();

    @RequestMapping(value = "/push", method = RequestMethod.GET)
    public void refresh(){
        //pusherList = new ArrayList<String>();
        pusherList.clear();
        //liveList = new ArrayList<>(String);
        liveList.clear();

    }




    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    List<String> pusherList;
    //@Scheduled(cron = "0/3 * * * * ? ")
    public void record(){
        String[] easyDarwinHosts = darwinHost.split(",");
        Arrays.stream(easyDarwinHosts).distinct().forEach(s->record(s));
    }



    public void recordStream(String darwinStrem,String darwinUserPwd){
        List<String> newList = new ArrayList<String>();
        newList.add(darwinStrem);
        if(pusherList.size() ==0 && newList.size()>0){
            newList.stream().forEach(s->respRecorder.recording(s,darwinUserPwd));
            pusherList.addAll(newList);
        }
        if(pusherList.size()>0 && newList.size()>0){
            newList.removeAll(pusherList);
            if(newList.size()>0){
                newList.stream().forEach(s->respRecorder.recording(s,darwinUserPwd));
                pusherList.addAll(newList);
            }

        }

    }


    public void record(String darwin){
        //System.out.println("scanning easydarwin ... ");
        String json = restTemplate.getForObject(darwin,String.class);
        JSONObject jsonObject = JSONObject.parseObject(json);
        int total = jsonObject.getIntValue("total");

        List<String> newList = new ArrayList<String>();

        if(total > 0) {
            JSONArray jSONArray = jsonObject.getJSONArray("rows");
            jSONArray.stream().map(jo->{
                JSONObject jo1 = (JSONObject) jo;
                return ((JSONObject) jo).getString("source");
            }).forEach(s->newList.add(s));
        }

        if(pusherList.size() ==0 && newList.size()>0){
            //System.out.println("start from 0");
            newList.stream().forEach(s->respRecorder.recording(s,"passwordhere"));
//            newList.stream().forEach(s->System.out.println("new list " + s));
//            DarwinChangeEvent event = new DarwinChangeEvent(this,newList);
//            applicationContext.publishEvent(event);
            pusherList.addAll(newList);
        }

        if(pusherList.size()>0 && newList.size()>0){
            //System.out.println("start from somewhere ... ");
            //newList.stream().forEach(s->System.out.println("new list " + s));
            newList.removeAll(pusherList);
            if(newList.size()>0){
                newList.stream().forEach(s->respRecorder.recording(s,"passwordhere"));
                //applicationContext.publishEvent(new DarwinChangeEvent(this,newList));
                pusherList.addAll(newList);
            }

        }

    }


    @Autowired
    List<String> liveList;
    //@Scheduled(cron = "0/10 * * * * ? ")
    public void pushToHLS(){
        String[] easyDarwinHosts = darwinHost.split(",");
        Arrays.stream(easyDarwinHosts).distinct().forEach(s->getPushers(s,""));
    }

    public void getPushers(String darwin,String darwinUserPwd) {
        System.out.println("scheduled job running");
        List<String> tobelive = new ArrayList<String>();
        String json = restTemplate.getForObject(darwin, String.class);
        JSONObject jsonObject = JSONObject.parseObject(json);
        int total = jsonObject.getIntValue("total");
        if (total > 0) {
            JSONArray jSONArray = jsonObject.getJSONArray("rows");
            jSONArray.stream().map(jo -> {
                JSONObject jo1 = (JSONObject) jo;
                return jo1;
            }).forEach(s -> tobelive.add(s.getString("source")));
        }
        tobelive.removeAll(liveList);
        if (tobelive.size() > 0) {
            tobelive.stream().forEach(s -> ffMPEGUtils.push(s, "",rtmpserver + StringUtils.substringBefore(StringUtils.substringAfterLast(s, "/"), ".sdp"),darwinUserPwd));
            liveList.addAll(tobelive);
        }
    }


    public void push(String darwinStream,String username,String darwinUserPwd) {
        System.out.println("scheduled job running");
        List<String> tobelive = new ArrayList<String>();
        tobelive.add(darwinStream);
        tobelive.removeAll(liveList);
        if (tobelive.size() > 0) {
            tobelive.stream().forEach(s -> ffMPEGUtils.push(s, username,rtmpserver + StringUtils.substringBefore(StringUtils.substringAfterLast(s, "/"), ".sdp"),darwinUserPwd));
            liveList.addAll(tobelive);
        }
    }


}