package com.kadan.lab.tester;


import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XML2JSON {

    public static void main(String[] args){

        Map<String,String> liveMap = new HashMap<String,String>();

        String xml = new RestTemplate().getForObject("http://139.219.142.18/stat",String.class);

        JSONArray jsonArray = org.json.XML.toJSONObject(xml).getJSONObject("rtmp").getJSONObject("server")
                .getJSONArray("application");

        for(int i=0; i<jsonArray.length(); i++){

            int clients = jsonArray.getJSONObject(i).getJSONObject("live").getInt("nclients");
            if(clients<=0)
                continue;
            JSONObject liveObject = jsonArray.getJSONObject(i).getJSONObject("live");
            String hlsname = jsonArray.getJSONObject(i).getString("name");
            System.out.println("name " + hlsname);
            if(clients == 1){
                JSONObject jsonObject = liveObject.getJSONObject("stream");

                String output = liveObject.getJSONObject("stream").getJSONObject("client").getString("address") + "/" + jsonObject.getString("name");
                System.out.println("output" + output);
                liveMap.put(hlsname,output);
            }else {


                JSONArray jsonArray1 = liveObject.getJSONArray("stream");

                for (int m = 0; m < jsonArray1.length(); m++) {
                    String output = jsonArray1.getJSONObject(m).getJSONObject("client").getString("address") + "/" + jsonArray1.getJSONObject(m).getString("name");
                    liveMap.put(hlsname, output);
                    System.out.println("output >>> " + output);
                }
            }


        }

    }

}
