//package com.kadan.lab.tester;
//
//import org.bytedeco.javacv.CanvasFrame;
//import org.bytedeco.javacv.OpenCVFrameGrabber;
//
//import javax.swing.*;
//
//public class CameraTester {
//    public static void main(String[] args) throws Exception, InterruptedException
//    {
//        OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
//        grabber.start();
//        CanvasFrame canvas = new CanvasFrame("Kadan camera");
//        canvas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        canvas.setAlwaysOnTop(true);
//        while(true)
//        {
//            if(!canvas.isDisplayable())
//            {
//                grabber.stop();
//                System.exit(2);
//            }
//            canvas.showImage(grabber.grab());
//            Thread.sleep(50);
//        }
//    }
//}
