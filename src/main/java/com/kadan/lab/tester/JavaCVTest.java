//package com.kadan.lab.tester;
//
//import org.bytedeco.javacpp.*;
//import org.bytedeco.javacv.*;
//import org.bytedeco.javacv.FrameGrabber.Exception;
//
//import javax.swing.*;
//import java.io.File;
//import java.math.RoundingMode;
//import java.net.MalformedURLException;
//import java.util.Map;
//import java.util.concurrent.TimeUnit;
//
//import static org.bytedeco.javacpp.opencv_core.Mat;
//import static org.bytedeco.javacpp.opencv_imgproc.COLOR_BGRA2GRAY;
//
//public class JavaCVTest {
//
//	public static void main(String[] args) throws Exception, InterruptedException, MalformedURLException {
//		//testCamera();
//		//testCamera1();
//		testVideo();
//	}
//
//    private static void showFrames(String winTitle, FrameGrabber grabber) throws Exception, InterruptedException {
//        CanvasFrame canvas = new CanvasFrame(winTitle,1);//新建一个窗口
//        canvas.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//        canvas.setAlwaysOnTop(true);
//        while (true) {
//            if (!canvas.isVisible()) {
//                break;
//            }
//            Frame frame = grabber.grab();
//            canvas.showImage(frame);
//            Thread.sleep(50);//50毫秒刷新一次图像
//        }
//    }
//
//    private void showFramesWithFace(String winTitle, FrameGrabber grabber) throws Exception, InterruptedException {
//        OpenCVFrameConverter.ToMat convertToMat = new OpenCVFrameConverter.ToMat();
//        File fileAbsolutePath = new File(ClassLoader.getSystemClassLoader().getResource("data/lbpcascade_frontalface_improved.xml").getFile());
//        //opencv_objdetect.CvHaarClassifierCascade face_cascade=opencv_objdetect.cvLoadHaarClassifierCascade(fileAbsolutePath.getAbsolutePath(),new opencv_core.CvSize(0,0));
//        opencv_objdetect.CascadeClassifier face_cascade = new opencv_objdetect.CascadeClassifier(fileAbsolutePath.getAbsolutePath());
//        opencv_core.RectVector faces = new opencv_core.RectVector();
//        CanvasFrame canvas = new CanvasFrame(winTitle,1);//新建一个窗口
//        canvas.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//        canvas.setAlwaysOnTop(true);
//        while (true) {
//            if (!canvas.isVisible()) {
//                break;
//            }
//            Frame frame = grabber.grab();
//            Mat mat = convertToMat.convert(frame);
//            if (mat.empty())
//                continue;
//            Mat videoMatGray = new Mat();
//            opencv_imgproc.cvtColor(mat, videoMatGray, COLOR_BGRA2GRAY);
//            opencv_imgproc.equalizeHist(videoMatGray, videoMatGray);
//            //int[] rejectLevels = new int[0];
//            //double[] levelWeights = new double[0];
//            face_cascade.detectMultiScale(videoMatGray, faces);
//            for (int i = 0; i < faces.size(); i++) {
//                opencv_core.Rect face = faces.get(i);
//                opencv_imgproc.rectangle(mat, face, opencv_core.Scalar.RED, 4, 8, 0);
//            }
//
//            //opencv_highgui.imshow(winTitle, mat);
//            //opencv_highgui.waitKey(30);
//            canvas.showImage(convertToMat.convert(mat));
//            Thread.sleep(30);//50毫秒刷新一次图像
//        }
//    }
//
//
//    public static void testCamera() throws InterruptedException, Exception {
//        OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
//        grabber.setImageWidth(1280);
//        grabber.setImageHeight(720);
//        grabber.start();   //开始获取摄像头数据
//        showFrames("Camera", grabber);
//        grabber.stop();
//        grabber.close();
//    }
//
//
//    public static void testCamera1() throws Exception, InterruptedException {
//        VideoInputFrameGrabber grabber = new VideoInputFrameGrabber(0);
//        grabber.start();   //开始获取摄像头数据
//        showFrames("Camera", grabber);
//        grabber.stop();
//        grabber.close();
//    }
//
//
//    public static void testCamera2() throws Exception, InterruptedException, MalformedURLException {
//        IPCameraFrameGrabber grabber = new IPCameraFrameGrabber("http://admin:12345@192.0.0.64:554/MPEG-4/ch1/main/av_stream", 30, 30, TimeUnit.SECONDS);
//        grabber.start();
//        showFrames("IPCamera", grabber);
//        grabber.stop();
//        grabber.close();
//    }
//
//
//    public static void testVideo() throws Exception, InterruptedException, MalformedURLException {
//        //FFmpegFrameGrabber grabber = FFmpegFrameGrabber.createDefault("rtmp://127.0.0.1:12580/live/app"); //这里也可以是本地文件，也可以网络文件。如：rtmp://127.0.0.1:12580/live/app
//        //FFmpegFrameGrabber grabber = FFmpegFrameGrabber.createDefault("rtmp://live.hkstv.hk.lxdns.com/live//hks"); //这里也可以是本地文件，也可以网络文件。如：rtmp://127.0.0.1:12580/live/app
//        //FFmpegFrameGrabber grabber = FFmpegFrameGrabber.createDefault("rtmp://127.0.0.1/live/"); //这里也可以是本地文件，也可以网络文件。如：rtmp://127.0.0.1:12580/live/app
//        FFmpegFrameGrabber grabber = FFmpegFrameGrabber.createDefault("rtsp://139.219.142.18/test0.sdp"); //这里也可以是本地文件，也可以网络文件。如：rtmp://127.0.0.1:12580/live/app
//        grabber.setImageWidth(600);
//        grabber.setImageHeight(400);
//        grabber.start();
//        showFrames("Video", grabber);
//        grabber.stop();
//        grabber.close();
//    }
//
//
//    public void testFaceRecognize() throws Exception, InterruptedException, MalformedURLException, FrameRecorder.Exception {
//        OpenCVFrameGrabber grabber = OpenCVFrameGrabber.createDefault(0);
//        grabber.start();
//        showFramesWithFace("Video", grabber);
//        grabber.stop();
//        grabber.close();
//    }
//
//
//    public void testRecordCamera() throws Exception, InterruptedException, MalformedURLException, FrameRecorder.Exception {
//        // Preload the opencv_objdetect module to work around a known bug.
//        String str = Loader.load(opencv_objdetect.class);
//        System.out.println(str);
//
//        FrameGrabber grabber = FrameGrabber.createDefault(0);
//        grabber.start();
//        OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();//转换器
//        opencv_core.IplImage grabbedImage = converter.convert(grabber.grab());//抓取一帧视频并将其转换为图像，至于用这个图像用来做什么？加水印，人脸识别等等自行添加
//        int width = grabbedImage.width();
//        int height = grabbedImage.height();
//
//        //String outputFile = "d:\\record.mp4";
//        String outputFile="rtmp://127.0.0.1:12580/live/mycamera";
//        FrameRecorder recorder = FrameRecorder.createDefault(outputFile, width, height); //org.bytedeco.javacv.FFmpegFrameRecorder
//        System.out.println(recorder.getClass().getName());//org.bytedeco.javacv.FFmpegFrameRecorder
//        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);// avcodec.AV_CODEC_ID_H264，编码
//        recorder.setFormat("flv");//封装格式，如果是推送到rtmp就必须是flv封装格式
//        recorder.setFrameRate(25);
//        recorder.start();//开启录制器
//        long startTime = 0;
//        long videoTS;
//        CanvasFrame frame = new CanvasFrame("camera", CanvasFrame.getDefaultGamma() / grabber.getGamma()); //2.2/2.2=1
//        //frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//        frame.setAlwaysOnTop(true);
//        Frame rotatedFrame;
//        while (frame.isVisible() && (grabbedImage = converter.convert(grabber.grab())) != null) {
//            rotatedFrame = converter.convert(grabbedImage);
//            frame.showImage(rotatedFrame);
//            if (startTime == 0) {
//                startTime = System.currentTimeMillis();
//            }
//            videoTS = (System.currentTimeMillis() - startTime) * 1000;//这里要注意，注意位
//            System.out.println();
//            recorder.setTimestamp(videoTS);
//            recorder.record(rotatedFrame);
//            Thread.sleep(40);
//        }
//        recorder.stop();
//        recorder.release();
//        frame.dispose();
//        grabber.stop();
//        grabber.close();
//    }
//
//
//}
