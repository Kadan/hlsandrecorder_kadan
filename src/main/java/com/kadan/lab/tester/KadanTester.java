package com.kadan.lab.tester;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.springframework.util.DigestUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class KadanTester {

    public static void main(String[] args) {
       System.out.println(DigestUtils.md5DigestAsHex("kadan1010".getBytes()));

//        new KadanTester().send();
    }


    public boolean send(){

        try {
            //RunProcessFunction func = new RunProcessFunction();
            //func.setWorkingDirectory("/Users/lijunjie/easyDarwin/ffmpeg/bin");
            FFmpeg ffmpeg = new FFmpeg("/usr/local/bin/ffmpeg");
            FFprobe ffprobe = new FFprobe("/usr/local/bin/ffprobe");

            FFmpegBuilder builder =
                    new FFmpegBuilder()
//                        .readAtNativeFrameRate()
                            //.addExtraArgs("-rtsp_transport tcp")
                            .setInput("rtsp://139.219.142.18:554/stream0.sdp")
                            //.addExtraArgs("-bsf:a aac_adtstoasc")
                            //.addExtraArgs("-bsf:a aac_adtstoasc")
                            //.addExtraArgs("-c:v copy")
                            //.setAudioCodec("libmp3lame")

                            //.addExtraArgs("-bsf:a aac_adtstoasc")
                            //.setVideoCodec("libx264")

                            //.setVideoCodec("libx264")


                            //.setPreset("ultrafast")
                            //.setConstantRateFactor(20)
                            //.setAudioCodec("libmp3")
                            //.setAudioFilter("aac_adtstoasc")

                            .addOutput("rtmp://139.219.142.18:10009/hls/kadan0")
                            .setVideoCodec("copy")
                            .setAudioCodec("copy")
                            .setAudioChannels(1)
                            //.addExtraArgs("bsf:a aac_adtstoasc")
                            //.setAudioFilter(".addExtraArgs(\"-bsf:a aac_adtstoasc\")")
                            //.setAudioFilter("-bsf:a aac_adtstoasc")
                            //.setFormat("flv")
                            //.setAudioFilter("adtstoasc")
                            .setAudioBitStreamFilter("aac_adtstoasc")
                            .setFormat("flv")
//                        .addExtraArgs("-bsf:a aac_adtstoasc")
                            .done();


            FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);
            executor.createJob(builder).run();
            //new FFmpegExecutor(ffmpeg,ffprobe).createJob(builder1).run();
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

}
