package com.kadan.lab.tester;

import com.kadan.lab.hlslive.util.PasswordEncrypt;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

public class KadanTester1 {

    public static void main(String[] args) {

        String password = "admin";



        try {
            System.out.println("this is the password after encryption +++ " +  DigestUtils.md5DigestAsHex(password.getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
        }

//        String recordDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyy-MM-dd-HH-mm"));
//        String outputFile = "/xxx/xx/xxx/" + recordDateTime + ".mp4";
//
//        System.out.println("outputFile" + outputFile);

        //new KadanTester1().send();
    }


    public boolean send(){

        try {
            //RunProcessFunction func = new RunProcessFunction();
            //func.setWorkingDirectory("/Users/lijunjie/easyDarwin/ffmpeg/bin");
            FFmpeg ffmpeg = new FFmpeg("/usr/local/bin/ffmpeg");
            FFprobe ffprobe = new FFprobe("/usr/local/bin/ffprobe");


            FFmpegBuilder builder1 =
                    new FFmpegBuilder()
//                        .readAtNativeFrameRate()
                            .setInput("rtsp://139.219.142.18/test0.sdp")
                            .overrideOutputFiles(false)
                            //.addExtraArgs("-bsf:a aac_adtstoasc")
                            //.addExtraArgs("-bsf:a aac_adtstoasc")
                            //.addExtraArgs("-c:v copy")
                            //.setAudioCodec("libmp3lame")

                            //.addExtraArgs("-bsf:a aac_adtstoasc")
                            //.setVideoCodec("libx264")

                            //.setVideoCodec("libx264")
//.setStartOffset(40, TimeUnit.SECONDS)

                            //.setPreset("ultrafast")
                            //.setConstantRateFactor(20)
                            //.setAudioCodec("libmp3")
                            //.setAudioFilter("aac_adtstoasc")
                            .addOutput("/Users/lijunjie/KadanLAB/output.mp4")
                            .setDuration(10, TimeUnit.SECONDS)
                            .setVideoCodec("copy")
                            .setAudioCodec("copy")
                            .setAudioChannels(1)
                            //.addExtraArgs("bsf:a aac_adtstoasc")
                            //.setAudioFilter(".addExtraArgs(\"-bsf:a aac_adtstoasc\")")
                            //.setAudioFilter("-bsf:a aac_adtstoasc")
                            //.setFormat("flv")
                            //.setAudioFilter("adtstoasc")
                            .setAudioBitStreamFilter("aac_adtstoasc")
                            .setFormat("mp4")

//                        .addExtraArgs("-bsf:a aac_adtstoasc")
                            .done();

            new FFmpegExecutor(ffmpeg,ffprobe).createJob(builder1).run();
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

}
